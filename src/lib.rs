pub mod commands;
mod vm;

pub use vm::State;

#[derive(Debug, Copy, Clone)]
pub enum Status {
  Failure,
  Success,
  ReturnFailure,
  ReturnSuccess,
}

impl Status {
  pub fn is_return(self) -> bool {
    use Status::*;

    match self {
      Failure | Success => false,
      ReturnFailure | ReturnSuccess => true,
    }
  }

  pub fn no_return(self) -> Self {
    use Status::*;

    match self {
        x@Failure | x@Success => x,
        ReturnFailure => Failure,
        ReturnSuccess => Success,
    }
  }
}

impl std::ops::Deref for Status {
  type Target = usize;

  fn deref(&self) -> &usize {
    use Status::*;

    static ONE: usize = 1;
    static ZERO: usize = 0;
    match *self {
      Failure | ReturnFailure => &ZERO,
      Success | ReturnSuccess => &ONE,
    }
  }
}

impl std::ops::Not for Status {
  type Output = Status;

  fn not(self) -> Self::Output {
    use Status::*;

    match self {
      Failure => Success,
      Success => Failure,
      ReturnFailure => ReturnSuccess,
      ReturnSuccess => ReturnFailure,
    }
  }
}

impl std::ops::Neg for Status {
  type Output = Status;

  fn neg(self) -> Self::Output {
    use Status::*;

    match self {
      Failure => ReturnFailure,
      Success => ReturnSuccess,
      ReturnFailure => Failure,
      ReturnSuccess => Success,
    }
  }
}

impl From<bool> for Status {
  fn from(b: bool) -> Status {
    if b {
      Status::Success
    } else {
      Status::Failure
    }
  }
}

pub trait Command {
  fn get_name(&self) -> &'static str;

  fn run(&self, state: &mut State, status: Status, args: &[String]) -> Status;
}

impl std::fmt::Debug for Command {
  fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
    write!(fmt, "Command: {}", self.get_name())
  }
}
