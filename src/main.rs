extern crate prexcl;

fn main() {
  let mut args = std::env::args();
  let _self_name = args.next();
  let prog_name = args.next();
  let program = if let Some(x) = prog_name {
    use std::error::Error;
    use std::fs::File;
    use std::io::prelude::*;
    use std::path::Path;
    let path = Path::new(&x);
    let display = path.display();
    let mut file = match File::open(&path) {
      Err(why) => panic!("couldn't open {}: {}", display, why.description()),
      Ok(file) => file,
    };
    let mut s = String::new();
    match file.read_to_string(&mut s) {
        Err(why) => panic!("couldn't read {}: {}", display, why.description()),
        Ok(_) => s,
    }
  } else {
    panic!("usage: prexcl <filename>")
  };
  let mut state = prexcl::State::new();
  {
    use prexcl::commands::*;
    state.register_command(Return);
    state.register_command(Loop);
    state.register_command(Nop);
    state.register_command(Println);
    state.register_command(Load);
    state.register_command(Store);
    state.register_command(Exec);
  }
  let parsed_program = state.parse(program.chars());
  println!("{:?}", parsed_program);
  state.call(&parsed_program);
}
