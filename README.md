Prexcl - Pre-Voxel Computer Language
====================================

Prexcl is an esoteric proof-of-concept programming language. It's being used to
test some concepts for Voxcl.

"Prexcl" is pronounced "prex-cel"; Similarly, "voxcl" is pronounced "voxel".
